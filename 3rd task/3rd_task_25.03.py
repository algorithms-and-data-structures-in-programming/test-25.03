class Solution:
    def mergeSimilarItems(self, items1, items2):
        ret = []

        for v1, w1 in items1:
            found = False
            for i, (val, wt) in enumerate(ret):
                if val == v1:
                    ret[i][1] += w1
                    found = True
                    break
            if not found:
                ret.append([v1, w1])

        for v2, w2 in items2:
            found = False
            for i, (val, wt) in enumerate(ret):
                if val == v2:
                    ret[i][1] += w2
                    found = True
                    break
            if not found:
                ret.append([v2, w2])

        ret.sort(key=lambda x: x[0])

        return ret


solution = Solution()
items1 = [[1, 1], [3, 2], [2, 3]]
items2 = [[2, 1], [3, 2], [1, 3]]
print(solution.mergeSimilarItems(items1, items2))
