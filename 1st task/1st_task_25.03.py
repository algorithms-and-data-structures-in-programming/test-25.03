class Solution:
    def kidsWithCandies(self, candies, extraCandies):
        max_amount = max(candies)
        ans = []

        for i in range(len(candies)):
            if candies[i] + extraCandies >= max_amount:
                ans.append(True)
            else:
                ans.append(False)

        return ans


solution = Solution()
candies = [12, 1, 12]
extraCandies = 10
print(solution.kidsWithCandies(candies, extraCandies))
