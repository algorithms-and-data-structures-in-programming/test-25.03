class Solution:
    def sortPeople(self, names, heights):
        people = []
        for i in range(len(names)):
            person = (names[i], heights[i])
            people.append(person)

        def height(person):
            return person[1]

        people.sort(key=height, reverse=True)

        namees = []
        for person in people:
            namees.append(person[0])

        return namees


solution = Solution()
names = ["Mary", "John", "Emma"]
heights = [180, 165, 170]
print(solution.sortPeople(names, heights))
